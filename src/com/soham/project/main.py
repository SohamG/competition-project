'''
@author "SohamG"
'''

import shelve
from tkinter import *
from tkinter.messagebox import showinfo
from src.com.soham.project.Employee import Employee
import time
import tkinter.ttk
# s = None
# db = None
# try : db = shelve.open(s)
# except TypeError :
#     inputwindow = Tk();
#     Label(inputwindow, text="Enter company name").pack()
#     p = Entry(inputwindow)
#     p.pack()
#     Button(inputwindow, text="Create database", command=lambda  : createdb(p.get())).pack()
#def createdb(a): db = shelve.open(a)

db = shelve.open("db")
class mainclass:
    def __init__(self, window):
        #self.ifaddemployee = ifaddemployee
        self.frame = Frame(window)
        self.window = window





    def printdetails(self, nm):
       #form = self.frame
       # form.pack()
        window4 = Tk()
        lb = Label(window4, text="Employee details are -\n" + "Name : " + db[nm].name + "\n " + "Position :" + " " + db[nm].pos + "\n " + "Salary :" + " " + db[nm].sal)
        lb.pack()
        #window.mainloop()
        # print("Employee details are -\n" +"\tName : "+db[nm]['name']+"\n \t"+"Position :"+" "+db[nm]['pos']+"\n \t"+"Salary :"+" " +db[nm]['sal'])


    def setdbval(self, empname, sal, pos, dob, date_joined):

        # db['empname'] = {'name':empname, 'sal':sal, 'pos':pos, 'dob':dob, 'date_joined':date_joined}
        db[empname] = Employee(empname, sal, pos, dob, date_joined)
        Label(text="Employee added").pack()
        # window = Tk()
        # fr = Frame(window)

        showinfo("New Employee Created!!",message="User created : " + db[empname].name)





    def addemployee(self, ifaddemployee):
        window2 = Tk()
        doagain = "Y"
        fr = Frame(window2)
        if ifaddemployee == "addemployee":
            while doagain == "Y":
                Label(fr, text="Name : ").grid(row=1, column=1)
                empname = Entry(fr)
                empname.grid(row=1, column=2)
                Label(fr, text="Salary : ").grid(row=2, column=1)
                sal = Entry(fr)
                sal.grid(row=2, column=2)
                Label(fr, text="Position").grid(row=3, column=1)
                pos = Entry(fr)
                pos.grid(row=3, column=2)
                Label(fr, text="DOB : ").grid(row=4, column=1)
                dob = Entry(fr)
                dob.grid(row=4, column=2)
                Label(fr, text="Date Joined : ").grid(row=5, column=1)
                date_joined = Entry(fr)
                date_joined.grid(row=5, column=2)
                #
                btn = Button(fr, text="Submit", command=(
                lambda: self.setdbval(empname=empname.get(), sal=sal.get(), pos=pos.get(), dob=dob.get(), date_joined=date_joined.get())))
                btn.grid(row=6, column=2)
                fr.pack()
                doagain = "N"

                Button(fr, text="Make another employee", command=(lambda: self.mainfunc("addemployee"))).grid(row=8, column=2)

            # print("Employee : " + db[empname].name + "\n \t" + "Position :" + " " + db[empname].pos + "\n \t" + "Salary :" + " " + db[empname].sal)


            window2.mainloop()
        else :
            #fr.destroy()
            self.mainfunc(ifaddemployee)


    def mainfunc(self, ifaddemployee):

        if (ifaddemployee == "addemployee"):
            self.addemployee(ifaddemployee)

        elif (ifaddemployee == "getdetails"):
            # window = Tk()
            self.getAllEmployeeData()
        elif (ifaddemployee == "getselecteddetails"):
                # window = Tk()
                window3 = Tk()
                Label(window3, text="Enter employee name : ").pack(side=TOP)
                nm = Entry(window3)
                nm.pack(side=TOP)

                butn = Button(window3, text="Submit", command=(lambda: self.printdetails(nm.get())))
                butn.pack(side=BOTTOM)
                #Label(window, text = "Employee details are -\n" +"\tName : "+db[nm]['name']+"\n \t"+"Position :"+" "+db[nm]['pos']+"\n \t"+"Salary :"+" " +db[nm]['sal']).pack()
                window.mainloop()
                #print("Employee details are -\n" +"\tName : "+db[nm]['name']+"\n \t"+"Position :"+" "+db[nm]['pos']+"\n \t"+"Salary :"+" " +db[nm]['sal'])


        else:
            print("Invalid option")


    def getAllEmployeeData(self):
        window1 = Tk()
        for key in db:
            # print("Employee : "+db[key]['name']+"\n \t"+"Position :"+" "+db[key]['pos']+"\n \t"+"Salary :"+" " +db[key]['sal'])

            # Label(window1, text="Employee : " + db[key].name + "\n \t" + "Position :" + " " + db[key].pos + "\n \t" + "Salary :" + " " + db[key].sal).pack(side=TOP)
            Label(window1, text="Employee : %s \nPosition : %s \nSalary : %i " % (db[key].name, db[key].pos, db[key].sal)).pack(side=TOP)
        window1.mainloop()




window = Tk()
window.title("Employee Manager")

# window.geometry()
# window.update()
# window.minsize(window.winfo_width()+10, window.winfo_height()+10)
obj = mainclass(window)
mainFr = Frame(window)
Button(mainFr, text="Add new Employee", command=(lambda : obj.mainfunc("addemployee"))).grid(row=1, column=1, padx=5, pady = 5)
Button(mainFr, text = "Display all employees\' details", command=(lambda : obj.mainfunc("getdetails"))).grid(row=1, column=2 ,padx=5, pady = 5)
Button(mainFr, text="Get details by name", command = lambda : obj.mainfunc("getselecteddetails")).grid(row=1, column=3, padx=5, pady = 5)
mainFr.pack()
window.mainloop()







